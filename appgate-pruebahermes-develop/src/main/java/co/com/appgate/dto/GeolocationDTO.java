/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author hermes.morales
 */
public class GeolocationDTO implements Serializable{
    
    private int id;
    private int ipFrom;
    private int ipTo;
    private CountryDTO countryDTO;
    private RegionDTO regionDTO;
    private CityDTO cityDTO;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private String timeZone;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the countryDTO
     */
    public CountryDTO getCountryDTO() {
        return countryDTO;
    }

    /**
     * @param countryDTO the countryDTO to set
     */
    public void setCountryDTO(CountryDTO countryDTO) {
        this.countryDTO = countryDTO;
    }

    /**
     * @return the regionDTO
     */
    public RegionDTO getRegionDTO() {
        return regionDTO;
    }

    /**
     * @param regionDTO the regionDTO to set
     */
    public void setRegionDTO(RegionDTO regionDTO) {
        this.regionDTO = regionDTO;
    }

    /**
     * @return the cityDTO
     */
    public CityDTO getCityDTO() {
        return cityDTO;
    }

    /**
     * @param cityDTO the cityDTO to set
     */
    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }

    /**
     * @return the latitude
     */
    public BigDecimal getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public BigDecimal getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone the timeZone to set
     */
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    /**
     * @return the ipFrom
     */
    public int getIpFrom() {
        return ipFrom;
    }

    /**
     * @param ipFrom the ipFrom to set
     */
    public void setIpFrom(int ipFrom) {
        this.ipFrom = ipFrom;
    }

    /**
     * @return the ipTo
     */
    public int getIpTo() {
        return ipTo;
    }

    /**
     * @param ipTo the ipTo to set
     */
    public void setIpTo(int ipTo) {
        this.ipTo = ipTo;
    }
    
}

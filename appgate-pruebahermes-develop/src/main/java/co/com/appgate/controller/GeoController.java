/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.controller;

import co.com.appgate.dto.GeolocationDTO;
import co.com.appgate.dto.Response;
import co.com.appgate.manager.FileManager;
import co.com.appgate.util.UtilFileReader;
import java.io.IOException;
import java.text.ParseException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author hermes.morales
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.PUT,RequestMethod.POST})
public class GeoController {
    
    FileManager fileManager = new FileManager();
    
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public String validar(){
        return "appgate service is running";
    }
    
    @RequestMapping(value = "/uploadfile", method = RequestMethod.PUT)
    public String uploadFile(@RequestBody String pathFile) throws Exception{
        fileManager.processFile(pathFile);
        return "process async runing!";
    }
    
    @RequestMapping(value = "/getinfoip/{ip}", method = RequestMethod.GET)
    public Response getinfoIp(@PathVariable String ip) throws ParseException{
        return fileManager.getInfoIp(ip);
    }
    
    @RequestMapping(value = "/testchangeip/{ip}", method = RequestMethod.GET)
    public String testchangeip(@PathVariable String ip) throws ParseException{
        return fileManager.testChangeIp(ip);
    }
    
}

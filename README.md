# appgate

appgate es una aplicación tipo springboot, esto le da ligereza y el atributo de ser liviana al momento de desplegar, para las operaciones tiene 3 servicios rest explicados a continuación:
1. http://localhost:8080/api/check
    Sirve para verificar si el servidor está arriba, es GET
2. http://localhost:8080/api/uploadfile
    Es para cargar el archivo plano con los registro de ip que se encuentran en el mismo, se envía en el body la url  del archivo y se carga mediante multihilos para tener un poco mas eficiencia en el tiempo de cargado y guardado, es PUT
3. http://localhost:8080/api/getinfoip/1.0.0.47
    Este sirve para verificar la información de una ip, trae todos los datos relacionados con esta, ciudad, pais, región etc, la ip se envía por parámetro en la url, en este caso se está enviando la ip 1.0.0.47, es GET

Se usaron las siguientes herramientas: aplicación standar springboot con maven, netbeans 12.3, api para conexión con base de datos api-persist-1.0-SNAPSHOT, base de datos H2, se probó con el archivo enviado y cargó perfectamente, para evitar redundancia en base de datos se crearon tablas maestras para ciudad, region y pais.
los scripts se adjuntan por correo.

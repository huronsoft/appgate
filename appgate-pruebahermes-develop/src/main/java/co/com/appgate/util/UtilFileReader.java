/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.util;

import co.com.appgate.dao.FileGeoDao;
import co.com.appgate.dto.GeolocationDTO;
import co.com.appgate.dto.LimitDTO;
import co.com.appgate.dto.Response;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author hermes.morales
 */
public class UtilFileReader {
    
    static FileGeoDao fileGeoDao = new FileGeoDao();
    
    private static final Logger logger = LogManager.getLogger(UtilFileReader.class);
    
    public static void upoadFile(String pathFile) throws ParseException{ 
        try (Stream<String> stream = Files.lines(Paths.get(pathFile))) {
            List<Object> list = stream.collect(Collectors.toList());
            int size = list.size();
            List<LimitDTO> limits = getLimitsFileToRead(size);
            UtilThread.executeMultiThread(limits, list);
        } catch (IOException e) {
            logger.error("Error uploading file, "+e.getMessage());
        } 
    }
    
    public static Response getInfoIp(String ip) throws ParseException {
        Response response = new Response();
        long ipInt = ConverterUtil.convertIpToLong(ip);
        GeolocationDTO geoLoc = fileGeoDao.finGeoLocationByIp(ipInt);
        if(geoLoc != null){
            response.setGeolocationDTO(geoLoc);
            response.setMessageResult("OK");
            return response;
        }
        response.setMessageResult("ip with number "+ip+" was not found");
        return response;
    }
    
    public static String testChangeIp(String ip){
        return String.valueOf(ConverterUtil.convertIpToLong(ip));
    }
    
    public static void writeFile() throws IOException{
        FileWriter myWriter = new FileWriter("C:/Users/hermes.morales/Documents/app/geotest.csv");
        int counter = 0;
        for(int i=0;i<15000000;i++){
            myWriter.write("\"19110540\",\"19110548\",\"TW\",\"TAIWAN PROVINCE OF CHINA\",\"TAOYUAN COUNTY\",\"TAOYUAN CITY\",\"24.9934\",\"121.297\",\"CHT.COM.TWCHUNGHWA TELECOM DATA COMMUNICATION BUSINESS GROUP\"\n");
            System.out.println(counter++);
        }
        myWriter.close();   
    }
    
    public static void getLengthFile(String pathFile){ 
        try (Stream<String> stream = Files.lines(Paths.get(pathFile))) {
            List<Object> list = stream.collect(Collectors.toList());
        } catch (IOException e) {
            logger.error("Error getting length file, "+e.getMessage());
        } 
    }
    
    private static List<LimitDTO> getLimitsFileToRead(int size){
        List<LimitDTO> limits = new ArrayList();
        LimitDTO limitDTO;
        int counter = 0;
        int firstLimit = 0;
        int secondLimit = 0;
        int partialLimit = getLimitByThread(size);       
        if(partialLimit == 0){
            limitDTO = new LimitDTO();
            limitDTO.setFirstLimit(firstLimit);
            limitDTO.setSecondLimit((size-1));
            limits.add(limitDTO);
            return limits;
        }
        int lengthFor = size/partialLimit;
        for(int i=0;i<lengthFor;i++){
           firstLimit = counter;
           secondLimit = counter + partialLimit;
           limitDTO = new LimitDTO();
           limitDTO.setFirstLimit(firstLimit);
           limitDTO.setSecondLimit(secondLimit-1);
           limits.add(limitDTO);
           counter=counter+partialLimit;
        }
        limits.get(limits.size()-1).setSecondLimit((size-1));
        return limits;
    }
    
    private static int getLimitByThread(int size){
        if(size >= 4000){
            return 2000;
        }
        return 0;
    }
    
}

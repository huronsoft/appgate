/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.util;

import co.com.huronsoft.api.entity.Setter;
import co.com.appgate.dto.CityDTO;
import co.com.appgate.dto.CountryDTO;
import co.com.appgate.dto.GeolocationDTO;
import co.com.appgate.dto.RegionDTO;
import co.com.appgate.util.Constant.Sequence;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hermes.morales
 */
public class ConverterUtil {
    
    public static GeolocationDTO convertSetterToGeoLoc(List<Setter> setters) throws ParseException{
        GeolocationDTO geolocationDTO = new GeolocationDTO();
        setters.stream().map(it -> {
            if(it.getNameColumn().equals(Constant.GeoLocTable.ID.value())){
                geolocationDTO.setId(Integer.parseInt(it.getValueColumn()));
            }
            return it;
        }).map(it -> {
            if(it.getNameColumn().equals(Constant.GeoLocTable.IDCITY.value())){
                CityDTO cityDTO = new CityDTO();
                cityDTO.setId(Integer.parseInt(it.getValueColumn()));
                geolocationDTO.setCityDTO(cityDTO);
            }
            return it;
        }).map(it -> {
            if(it.getNameColumn().equals(Constant.GeoLocTable.IDCOUNTRY.value())){
                CountryDTO countryDTO = new CountryDTO();
                countryDTO.setId(Integer.parseInt(it.getValueColumn()));
                geolocationDTO.setCountryDTO(countryDTO);
            }
            return it;
        }).map(it -> {
            if(it.getNameColumn().equals(Constant.GeoLocTable.IDREGION.value())){
                RegionDTO regionDTO = new RegionDTO();
                regionDTO.setId(Integer.parseInt(it.getValueColumn()));
                geolocationDTO.setRegionDTO(regionDTO);
            }
            return it;
        }).map(it -> {
            if(it.getNameColumn().equals(Constant.GeoLocTable.IPFROM.value())){
                geolocationDTO.setIpFrom(Integer.parseInt(it.getValueColumn()));
            }
            return it;
        }).map(it -> {
            if(it.getNameColumn().equals(Constant.GeoLocTable.IPTO.value())){
                geolocationDTO.setIpTo(Integer.parseInt(it.getValueColumn()));
            }
            return it;
        }).map(it -> {
            if(it.getNameColumn().equals(Constant.GeoLocTable.LATITUDE.value())){
                BigDecimal latitude = new BigDecimal(it.getValueColumn());
                geolocationDTO.setLatitude(latitude);
            }
            return it;
        }).map(it -> {
            if(it.getNameColumn().equals(Constant.GeoLocTable.LONGITUDE.value())){
                BigDecimal longitude = new BigDecimal(it.getValueColumn());
                geolocationDTO.setLongitude(longitude);
            }
            return it;
        }).filter(it -> (it.getNameColumn().equals(Constant.GeoLocTable.TIMEZONE.value()))).forEachOrdered(it -> {
            geolocationDTO.setTimeZone(it.getValueColumn());
        });
        return geolocationDTO;
    }
    
    public static CityDTO convertSetterToCity(List<Setter> setters) throws ParseException{
        CityDTO cityDTO = new CityDTO();
        setters.stream().map(it -> {
            if(it.getNameColumn().equals(Constant.CityTable.ID.value())){
                cityDTO.setId(Integer.parseInt(it.getValueColumn()));
            }
            return it;
        }).filter(it -> (it.getNameColumn().equals(Constant.CityTable.NAME.value()))).forEachOrdered(it -> {
            cityDTO.setName(it.getValueColumn());
        });
        return cityDTO;
    }
    
    public static CountryDTO convertSetterToCountry(List<Setter> setters) throws ParseException{
        CountryDTO countryDTO = new CountryDTO();
        if(setters != null && !setters.isEmpty()){
            setters.stream().map(it -> {
                if(it.getNameColumn().equals(Constant.CountryTable.ID.value())){
                    countryDTO.setId(Integer.parseInt(it.getValueColumn()));
                }
                return it;
            }).map(it -> {
                if(it.getNameColumn().equals(Constant.CountryTable.NAME.value())){
                    countryDTO.setName(it.getValueColumn());
                }
                return it;
            }).filter(it -> (it.getNameColumn().equals(Constant.CountryTable.CODE.value()))).forEachOrdered(it -> {
                countryDTO.setCode(it.getValueColumn());
            });
            return countryDTO;
        }   
        return null;
    }
    
    public static RegionDTO convertSetterToRegion(List<Setter> setters) throws ParseException{
        RegionDTO regionDTO = new RegionDTO();
        setters.stream().map(it -> {
            if(it.getNameColumn().equals(Constant.RegionTable.ID.value())){
                regionDTO.setId(Integer.parseInt(it.getValueColumn()));
            }
            return it;
        }).filter(it -> (it.getNameColumn().equals(Constant.RegionTable.NAME.value()))).forEachOrdered(it -> {
            regionDTO.setName(it.getValueColumn());
        });
        return regionDTO;
    }
    
    public static GeolocationDTO convertLineToGeoLoc(String line){
        if(line != null){
            GeolocationDTO geolocationDTO = new GeolocationDTO();
            CountryDTO countryDTO = new CountryDTO();
            String[] arr = line.split(",");
            for(int i=0;i<arr.length;i++){
                String item = arr[i].replaceAll("\"", "").replaceAll("'", " ");
                if(i == Constant.PositionField.CITY.value()){
                    CityDTO cityDTO = new CityDTO();
                    cityDTO.setName(item.trim().toUpperCase());
                    geolocationDTO.setCityDTO(cityDTO);
                }
                if(i == Constant.PositionField.COUNTRY.value()){
                    countryDTO.setName(item.trim().toUpperCase());
                }
                if(i == Constant.PositionField.COUNTRY_CODE.value()){
                    countryDTO.setCode(item.trim().toUpperCase());
                }
                if(i == Constant.PositionField.IPFROM.value()){
                    geolocationDTO.setIpFrom(Integer.parseInt(item.trim()));
                }
                if(i == Constant.PositionField.IPTO.value()){
                    geolocationDTO.setIpTo(Integer.parseInt(item));
                }
                if(i == Constant.PositionField.LATITUDE.value()){
                    BigDecimal latitude = new BigDecimal(item);
                    geolocationDTO.setLatitude(latitude);
                }
                if(i == Constant.PositionField.LONGITUDE.value()){
                     BigDecimal longitude = new BigDecimal(item);
                    geolocationDTO.setLongitude(longitude);
                }
                if(i == Constant.PositionField.REGION.value()){
                    RegionDTO regionDTO = new RegionDTO();
                    regionDTO.setName(item.trim().toUpperCase());
                    geolocationDTO.setRegionDTO(regionDTO);
                }
                if(i == Constant.PositionField.TIMEZONE.value()){
                    geolocationDTO.setTimeZone(item.trim().toUpperCase());
                }
            }
            geolocationDTO.setCountryDTO(countryDTO);
            return geolocationDTO;
        }
        return null;
    }
    
    public static String getInsertQueryCountry(CountryDTO countryDTO) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO COUNTRY VALUES (");
        query.append(Sequence.SEQ_COUNTRY.value());
        query.append(", ");
        query.append("'").append(countryDTO.getCode().replace("'", " ")).append("'");
        query.append(", ");
        query.append("'").append(countryDTO.getName().replace("'", " ")).append("'");
        query.append(")");
        return query.toString();
    }
    
    public static String getInsertQueryRegion(RegionDTO regionDTO) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO REGION VALUES (");
        query.append(Sequence.SEQ_REGION.value());
        query.append(", ");
        query.append("'").append(regionDTO.getName().replace("'", " ")).append("'");
        query.append(")");
        return query.toString();
    }
    
    public static String getInsertQueryCity(CityDTO cityDTO) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO CITY VALUES (");
        query.append(Sequence.SEQ_CITY.value());
        query.append(", ");
        query.append("'").append(cityDTO.getName().replace("'", " ")).append("'");
        query.append(")");
        return query.toString();
    }
     
    public static String getInsertQueryGeoLoc(GeolocationDTO geolocationDTO) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO GEOLOCATION VALUES (");
        query.append(Sequence.SEQ_GEO.value());
        query.append(", ");
        query.append(geolocationDTO.getIpFrom());
        query.append(", ");
        query.append(geolocationDTO.getIpTo());
        query.append(", ");
        query.append(geolocationDTO.getCountryDTO().getId());
        query.append(", ");
        query.append(geolocationDTO.getRegionDTO().getId());
        query.append(", ");
        query.append(geolocationDTO.getCityDTO().getId());
        query.append(", ");
        query.append(geolocationDTO.getLatitude());
        query.append(", ");
        query.append(geolocationDTO.getLongitude());
        query.append(", ");
        query.append("'").append(geolocationDTO.getTimeZone().replace("'", " ")).append("'");
        query.append(")");
        return query.toString();
    }
    
    public static long convertIpToLong(String ip){
        List<Long> segments = getSegmentsIp(ip);
        long firstSeg = segments.get(0);
        long secondSeg = segments.get(1);
        long thirdSeg = segments.get(2);
        long fourthSeg = segments.get(3);
        long firstPiece = firstSeg * Long.parseLong("16777216");
        long secondPiece = secondSeg * Long.parseLong("65536");
        long thirdPiece = thirdSeg * Long.parseLong("256");
        long fourthPiece = fourthSeg;     
        return (firstPiece+secondPiece+thirdPiece+fourthPiece);
    }
    
    private static List<Long> getSegmentsIp(String ip){ 
        char[] arr = ip.toCharArray();
        List<Long> segments = new ArrayList();
        StringBuilder ipBuilder = new StringBuilder();
        for(char it : arr){
            String item = String.valueOf(it);
            if(!item.equals(".")){
                ipBuilder.append(item);
            }
            if(item.equals(".")){
                segments.add(Long.parseLong(ipBuilder.toString()));
                ipBuilder = new StringBuilder();
            }
        } 
        segments.add(Long.parseLong(ipBuilder.toString()));
        return segments;
    }
    
}

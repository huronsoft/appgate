/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.util;

import co.com.appgate.dao.FileGeoDao;
import co.com.appgate.dto.GeolocationDTO;
import co.com.appgate.dto.LimitDTO;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
/**
 *
 * @author hermes.morales
 */
public class UtilRunnable implements Runnable {
    private Thread t;
    private final String threadName;
    List<Object> listGeo;
    LimitDTO limit;
    
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(UtilRunnable.class);

    UtilRunnable(String name, List<Object> list, LimitDTO limitDTO) {
       threadName = name;
       listGeo = list;
       limit = limitDTO;
    }

    @Override
    @SuppressWarnings({"SleepWhileInLoop", "null"})
    public void run() {
        try {
            List<GeolocationDTO> listGeoLocDTO = new ArrayList();
            for(int i=limit.getFirstLimit();i<=limit.getSecondLimit();i++){
                GeolocationDTO geolocationDTO = 
                        ConverterUtil.convertLineToGeoLoc(listGeo.get(i).toString());
                if(geolocationDTO != null){
                    listGeoLocDTO.add(geolocationDTO);
                }
            }
            System.out.println("thread in limit: "+limit.getSecondLimit());
            //save the list on data base
            FileGeoDao.saveFiles(listGeoLocDTO);
            Thread.sleep(50);
        } catch (InterruptedException | ParseException ex) {
            logger.error("Error running thread, "+ex.getMessage());
        }
    }

    public void start () {
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
    
  
}

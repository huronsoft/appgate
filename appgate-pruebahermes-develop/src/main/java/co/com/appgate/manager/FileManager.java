/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.manager;

import co.com.appgate.dto.GeolocationDTO;
import co.com.appgate.dto.Response;
import co.com.appgate.util.UtilFileReader;
import java.text.ParseException;

/**
 *
 * @author hermes.morales
 */
public class FileManager { 
    
    public void processFile(String pathFile) throws ParseException{
        UtilFileReader.upoadFile(pathFile);
    }
    
    public Response getInfoIp(String ip) throws ParseException{
        return UtilFileReader.getInfoIp(ip);
    }
    
    public String testChangeIp(String ip) throws ParseException{
        return UtilFileReader.testChangeIp(ip);
    }
    
}

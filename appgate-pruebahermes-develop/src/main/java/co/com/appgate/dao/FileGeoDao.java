/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.dao;

import co.com.huronsoft.api.dto.ConectorDTO;
import co.com.huronsoft.api.entity.Owner;
import co.com.huronsoft.api.entity.Scrypt;
import co.com.huronsoft.api.facade.Query;
import co.com.huronsoft.api.implement.ApiQuery;
import co.com.huronsoft.api.util.Constant;
import co.com.appgate.dto.CityDTO;
import co.com.appgate.dto.CountryDTO;
import co.com.appgate.dto.GeolocationDTO;
import co.com.appgate.dto.RegionDTO;
import co.com.appgate.util.ConverterUtil;
import co.com.appgate.util.UtilConection;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author hermes.morales
 */
public class FileGeoDao {

    public static void saveFiles(List<GeolocationDTO> listGeoLoc) throws ParseException{
        int counter = 0;
        for(GeolocationDTO it : listGeoLoc){
            counter++;
            GeolocationDTO geoLoc = getGeolocationDTOToCreate(it);
            createGeoLocation(geoLoc, counter);
        }
    }
    
    private static CountryDTO findCountryByName(CountryDTO countryDTO) throws ParseException {
        CountryDTO country = null;
        Scrypt script = new Scrypt(Constant.SqlEnum.SELECT.value(), "COUNTRY", null);
        script.setSimpleQuery("SELECT * FROM COUNTRY WHERE NAME = '"+countryDTO.getName().replace("'", " ")+"'");
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        List<Owner> listData = ap.getList(manualCon);
        if(listData != null){
            for(Owner it : listData){
                country = ConverterUtil.convertSetterToCountry(it.getSetters());
            }
        }        
        return country;
    }
    
    private static CountryDTO findCountryById(int idCountry) throws ParseException {
        CountryDTO country = null;
        Scrypt script = new Scrypt(Constant.SqlEnum.SELECT.value(), "COUNTRY", null);
        script.setSimpleQuery("SELECT * FROM COUNTRY WHERE ID = "+idCountry);
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        List<Owner> listData = ap.getList(manualCon);
        if(listData != null){
            for(Owner it : listData){
                country = ConverterUtil.convertSetterToCountry(it.getSetters());
            }
        }        
        return country;
    }
    
    private static RegionDTO findRegionByName(RegionDTO regionDTO) throws ParseException {
        RegionDTO region = null;
        Scrypt script = new Scrypt(Constant.SqlEnum.SELECT.value(), "REGION", null);
        script.setSimpleQuery("SELECT * FROM REGION WHERE NAME = '"+regionDTO.getName().replace("'", " ")+"'");
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        List<Owner> listData = ap.getList(manualCon);
        if(listData != null){
            for(Owner it : listData){
                region = ConverterUtil.convertSetterToRegion(it.getSetters());
            }
        }   
        return region;
    }
    
    private static RegionDTO findRegionById(int idRegion) throws ParseException {
        RegionDTO region = null;
        Scrypt script = new Scrypt(Constant.SqlEnum.SELECT.value(), "REGION", null);
        script.setSimpleQuery("SELECT * FROM REGION WHERE ID = "+idRegion);
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        List<Owner> listData = ap.getList(manualCon);
        if(listData != null){
            for(Owner it : listData){
                region = ConverterUtil.convertSetterToRegion(it.getSetters());
            }
        }   
        return region;
    }
    
    private static CityDTO findCityByName(CityDTO cityDTO) throws ParseException {
        CityDTO city = null;
        Scrypt script = new Scrypt(Constant.SqlEnum.SELECT.value(), "CITY", null);
        script.setSimpleQuery("SELECT * FROM CITY WHERE NAME = '"+cityDTO.getName()+"'");
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        List<Owner> listData = ap.getList(manualCon);
        if(listData != null){
            for(Owner it : listData){
                city = ConverterUtil.convertSetterToCity(it.getSetters());
            }
        }   
        return city;
    }
    
    private static CityDTO findCityById(int idCity) throws ParseException {
        CityDTO city = null;
        Scrypt script = new Scrypt(Constant.SqlEnum.SELECT.value(), "CITY", null);
        script.setSimpleQuery("SELECT * FROM CITY WHERE ID = "+idCity);
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        List<Owner> listData = ap.getList(manualCon);
        if(listData != null){
            for(Owner it : listData){
                city = ConverterUtil.convertSetterToCity(it.getSetters());
            }
        }   
        return city;
    }
    
    
    public static GeolocationDTO finGeoLocationByIp(long ip) throws ParseException {
        GeolocationDTO geolocationDTO = null;
        Scrypt script = new Scrypt(Constant.SqlEnum.SELECT.value(), "GEOLOCATION", null);
        script.setSimpleQuery("SELECT * FROM GEOLOCATION WHERE "+ip+" BETWEEN IPFROM AND IPTO");
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB(); 
        List<Owner> listData = ap.getList(manualCon);
        if(listData != null){
            for(Owner it : listData){
                geolocationDTO = ConverterUtil.convertSetterToGeoLoc(it.getSetters());
            }
        }   
        if(geolocationDTO != null){
            int idCity = geolocationDTO.getCityDTO().getId();
            int idCountry = geolocationDTO.getCountryDTO().getId();
            int idRegion = geolocationDTO.getRegionDTO().getId();
            geolocationDTO.setCityDTO(findCityById(idCity));
            geolocationDTO.setCountryDTO(findCountryById(idCountry));
            geolocationDTO.setRegionDTO(findRegionById(idRegion));
        }
        return geolocationDTO;
    } 
    
    private static GeolocationDTO getGeolocationDTOToCreate(GeolocationDTO geolocationDTO) throws ParseException{
        CountryDTO country = findCountryByName(geolocationDTO.getCountryDTO());
        if(country == null){
            country = geolocationDTO.getCountryDTO();
            createCountry(country);
            country = findCountryByName(geolocationDTO.getCountryDTO());
            geolocationDTO.setCountryDTO(country);
        }else{
            geolocationDTO.setCountryDTO(country);
        }
        RegionDTO region = findRegionByName(geolocationDTO.getRegionDTO());
        if(region == null){
            region = geolocationDTO.getRegionDTO();
            createRegion(region);
            region = findRegionByName(geolocationDTO.getRegionDTO());
            geolocationDTO.setRegionDTO(region);
        }else{
            geolocationDTO.setRegionDTO(region);
        }
        CityDTO city = findCityByName(geolocationDTO.getCityDTO());
        if(city == null){
            city = geolocationDTO.getCityDTO();
            createCity(city);
            city = findCityByName(geolocationDTO.getCityDTO());
            geolocationDTO.setCityDTO(city);
        }else{
            geolocationDTO.setCityDTO(city);
        }
        return geolocationDTO;
    }
    
    private static void createCountry(CountryDTO country){
        Scrypt script = new Scrypt(Constant.SqlEnum.INSERT.value(), "NONE", null);
        script.setSimpleQuery(ConverterUtil.getInsertQueryCountry(country));
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        ap.insert(manualCon);
    }
    
    private static void createRegion(RegionDTO region){
        Scrypt script = new Scrypt(Constant.SqlEnum.INSERT.value(), "NONE", null);
        script.setSimpleQuery(ConverterUtil.getInsertQueryRegion(region));
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        ap.insert(manualCon);
    }
    
    private static void createCity(CityDTO city){
        Scrypt script = new Scrypt(Constant.SqlEnum.INSERT.value(), "NONE", null);
        script.setSimpleQuery(ConverterUtil.getInsertQueryCity(city));
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        ap.insert(manualCon);
    }
    
    private static void createGeoLocation(GeolocationDTO geolocation, int counter){
        Scrypt script = new Scrypt(Constant.SqlEnum.INSERT.value(), "NONE", null);
        script.setSimpleQuery(ConverterUtil.getInsertQueryGeoLoc(geolocation));
        Query ap = new ApiQuery(script);
        ConectorDTO manualCon = UtilConection.getPropertiesDB();
        ap.insert(manualCon);
        System.out.println(counter+" saved");
    }
}

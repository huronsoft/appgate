/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.util;

import co.com.appgate.dao.FileGeoDao;
import co.com.appgate.dto.GeolocationDTO;
import co.com.appgate.dto.LimitDTO;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hermes.morales
 */
public class UtilThread {
    
    public static void executeMultiThread(List<LimitDTO> limits, List<Object> list) throws ParseException{
        
        for(LimitDTO it : limits){
            System.out.println("init limit"+it.getFirstLimit()+", final limit: " + it.getSecondLimit());
            UtilRunnable thread = new UtilRunnable("thread with limit: "+it.getSecondLimit(), list, it);
            thread.start();          
//            List<GeolocationDTO> listGeoLocDTO = new ArrayList();//            logger.error("Error running thread, "+ex.getMessage());
//            for(int i=it.getFirstLimit();i<=it.getSecondLimit();i++){
//                GeolocationDTO geolocationDTO =
//                        ConverterUtil.convertLineToGeoLoc(list.get(i).toString());
//                if(geolocationDTO != null){
//                    listGeoLocDTO.add(geolocationDTO);
//                }
//            }
//            FileGeoDao.saveFiles(listGeoLocDTO);
        }
        System.out.println("finish!");
    }
}

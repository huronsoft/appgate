/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.util;

/**
 *
 * @author hermes.morales
 */
public class Constant {
    
    public static int LIMIT_READ_FILE = 100;
    public static int PERCENT_BREAK_FILE = 10;
    
    public static enum CityTable {
        ID("ID"),  	
        NAME("NAME");
        private final String value;
        
        private CityTable(String value){
            this.value = value;
        }
        
        public String value() {
            return value;
        }
    }
    
    public static enum RegionTable {
        ID("ID"),  	
        NAME("NAME");
        private final String value;
        
        private RegionTable(String value){
            this.value = value;
        }
        
        public String value() {
            return value;
        }
    }
    
    public static enum CountryTable {
        ID("ID"),  	
        NAME("NAME"),
        CODE("CODE");
        private final String value;
        
        private CountryTable(String value){
            this.value = value;
        }
        
        public String value() {
            return value;
        }
    }
    
    public static enum GeoLocTable {
        ID("ID"),  	
        IPFROM("IPFROM"),
        IPTO("IPTO"),
        IDCOUNTRY("IDCOUNTRY"),
        IDREGION("IDREGION"),
        IDCITY("IDCITY"),
        LATITUDE("LATITUDE"),
        LONGITUDE("LONGITUDE"),
        TIMEZONE("TIMEZONE");
        private final String value;
        
        private GeoLocTable(String value){
            this.value = value;
        }
        
        public String value() {
            return value;
        }
    }

    public static enum PositionField {
        IPFROM(0),  	
        IPTO(1),
        COUNTRY_CODE(2),
        COUNTRY(3),
        REGION(4),
        CITY(5),
        LATITUDE(6),
        LONGITUDE(7),
        TIMEZONE(8);
        private final int value;
        
        private PositionField(int value){
            this.value = value;
        }
        
        public int value() {
            return value;
        }
    }
    
    //SEQ_COUNTRY.NEXTVAL
    public static enum Sequence {
        SEQ_COUNTRY("SEQ_COUNTRY"),  	
        SEQ_GEO("SEQ_GEO"),
        SEQ_REGION("SEQ_REGION"),
        SEQ_CITY("SEQ_CITY");
        private final String value;
        private final String NEXTVAL = ".NEXTVAL";
        
        private Sequence(String value){
            this.value = value;
        }
        
        public String value() {
            return value.concat(NEXTVAL);
        }

    }
    
}

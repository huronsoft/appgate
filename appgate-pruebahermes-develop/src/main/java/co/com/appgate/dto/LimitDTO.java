/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.dto;

import java.io.Serializable;

/**
 *
 * @author hermes.morales
 */
public class LimitDTO implements Serializable{
    
    private int firstLimit;
    private int secondLimit;
    /**
     * @return the firstLimit
     */
    public int getFirstLimit() {
        return firstLimit;
    }

    /**
     * @param firstLimit the firstLimit to set
     */
    public void setFirstLimit(int firstLimit) {
        this.firstLimit = firstLimit;
    }

    /**
     * @return the secondLimit
     */
    public int getSecondLimit() {
        return secondLimit;
    }

    /**
     * @param secondLimit the secondLimit to set
     */
    public void setSecondLimit(int secondLimit) {
        this.secondLimit = secondLimit;
    }
}

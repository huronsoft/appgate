/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.dto;

/**
 *
 * @author hermes.morales
 */
public class Response {
    
    private GeolocationDTO geolocationDTO;
    private String messageResult;

    /**
     * @return the messageResult
     */
    public String getMessageResult() {
        return messageResult;
    }

    /**
     * @param messageResult the messageResult to set
     */
    public void setMessageResult(String messageResult) {
        this.messageResult = messageResult;
    }

    /**
     * @return the geolocationDTO
     */
    public GeolocationDTO getGeolocationDTO() {
        return geolocationDTO;
    }

    /**
     * @param geolocationDTO the geolocationDTO to set
     */
    public void setGeolocationDTO(GeolocationDTO geolocationDTO) {
        this.geolocationDTO = geolocationDTO;
    }
}

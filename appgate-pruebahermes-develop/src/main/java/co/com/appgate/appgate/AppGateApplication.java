package co.com.appgate.appgate;

import co.com.appgate.controller.GeoController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@ComponentScan(basePackages = {"co.com.appgate.controller", "co.com.appgate.service", "co.com.appgate.dao", "co.com.appgate.manager"})
@EntityScan("co.com.appgate.entity")
@EnableJpaRepositories("co.com.appgate.dao")
public class AppGateApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppGateApplication.class, args);
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.appgate.util;

import co.com.huronsoft.api.dto.ConectorDTO;

/**
 *
 * @author hermes.morales
 */
public class UtilConection {
    
    public static ConectorDTO getPropertiesDB(){
        ConectorDTO manualCon = new ConectorDTO();
        manualCon.setJdbcDriver("org.h2.Driver");
        manualCon.setPassword("");
        manualCon.setUrlDbConection("jdbc:h2:~/test");
        manualCon.setUser("sa");
        return manualCon;
    }
}
